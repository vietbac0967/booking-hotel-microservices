package com.example.roomservice.enums;

public enum RoomType {
    Single,       // Phòng đơn
    Double,       // Phòng đôi
    Twin,         // Phòng có hai giường đơn
    Triple,       // Phòng ba
    Suite,        // Phòng sang trọng
    Family,       // Phòng gia đình
    Deluxe        // Phòng cao cấp
    ;

}
