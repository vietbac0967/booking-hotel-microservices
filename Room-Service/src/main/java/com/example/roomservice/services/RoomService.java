package com.example.roomservice.services;

import com.example.roomservice.enums.RoomStatus;
import com.example.roomservice.enums.RoomType;
import com.example.roomservice.models.Room;
import com.example.roomservice.repositories.RoomRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RoomService {
    private final RoomRepository roomRepository;
    public List<Room> findByPrice(double min, double max) {
        return roomRepository.findByPrice(min, max);
    }
    public List<Room> findByRoomType(String roomType){
        return roomRepository.findByRoomType(roomType);
    }
    public List<Room> findByRoomStatus (RoomStatus roomStatus){
        return roomRepository.findByRoomStatus(roomStatus);
    }
    public List<Room> numOfBed (int numOfBed){
        return roomRepository.numOfBed(numOfBed);
    }
    public List<Room> maxPeople (int maxPeople){
        return roomRepository.maxPeople(maxPeople);
    }
}
