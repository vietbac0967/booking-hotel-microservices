package com.example.roomservice.models;

import com.example.roomservice.enums.RoomStatus;
import com.example.roomservice.enums.RoomType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data @Entity @NoArgsConstructor @AllArgsConstructor
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long roomId;
    private RoomType roomType;
    private double price;
    private int numOfBed;
    private int maxPeople;
    private RoomStatus roomStatus;
    private Long userId;
}
