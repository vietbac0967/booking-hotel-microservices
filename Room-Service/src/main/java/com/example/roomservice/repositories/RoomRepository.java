package com.example.roomservice.repositories;

import com.example.roomservice.enums.RoomStatus;
import com.example.roomservice.enums.RoomType;
import com.example.roomservice.models.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoomRepository extends JpaRepository<Room,Long> {
    @Query("SELECT r FROM Room r WHERE r.price >= :min AND r.price <= :max")
    List<Room> findByPrice(double min, double max);
    @Query("SELECT r FROM Room r WHERE cast (r.roomType as String ) LIKE CONCAT('%', :roomType, '%')")
    List<Room> findByRoomType(String roomType);
    List<Room> findByRoomStatus(RoomStatus roomStatus);
    @Query("select  r from Room r where r.numOfBed = :numOfBed")
    List<Room> numOfBed(int numOfBed);
    @Query("select  r from Room r where r.maxPeople = :maxPeople")
    List<Room> maxPeople(int maxPeople);

    List<Room> getRoomsByUserId(Long userId);

}
