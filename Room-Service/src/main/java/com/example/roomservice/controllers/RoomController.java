package com.example.roomservice.controllers;

import com.example.roomservice.models.Room;
import com.example.roomservice.repositories.RoomRepository;
import com.example.roomservice.services.RoomService;
import lombok.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/rooms")
@RequiredArgsConstructor
public class RoomController {
    private final RoomRepository roomRepository;
    private final RoomService roomService;

    @PostMapping("")
    public ResponseEntity<Room> createRoom(@RequestBody Room room) {
        roomRepository.save(room);
        return ResponseEntity.ok(room);
    }

    @GetMapping("")
    public List<Room> getAll() {
        return roomRepository.findAll();
    }

    @GetMapping("/user")
    public List<Room> getRoomsForUser(@RequestParam("userId") Long id){
        return roomRepository.getRoomsByUserId(id);
    }

    @PostMapping("/delete/{id}")
    public ResponseEntity<String> deleteRoom(@PathVariable("id") Long id) {
        var optionalOrder = roomRepository.findById(id);
        // Check if the order exists
        if (optionalOrder.isPresent()) {
            // If the order exists, delete it and return a success message
            roomRepository.deleteById(id);
            return ResponseEntity.ok("Delete success with order id: " + id);
        } else {
            // If the order does not exist, return a 404 Not Found response
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Order with id " + id + " not found");
        }
    }

    @PostMapping("/update")
    public ResponseEntity<Room> updateOrder(@RequestBody Room room) {
        Long orderId = room.getRoomId();
        if (orderId == null || !roomRepository.existsById(orderId)) {
            return ResponseEntity.notFound().build();
        } else {
            // Retrieve the existing order from the repository
            Room existingOrder = roomRepository.findById(orderId).get();
            existingOrder.setPrice(room.getPrice());
            // Save the updated order back to the repository
            Room updatedOrder = roomRepository.save(existingOrder);
            // Return the updated order with HTTP status OK
            return ResponseEntity.ok(updatedOrder);
        }
    }

    @GetMapping("/rooms/price")
    public ResponseEntity<List<Room>> findByPrice(
            @RequestParam("min") double min,
            @RequestParam("max") double max
    ) {
        List<Room> rooms = roomService.findByPrice(min, max);

        if (!rooms.isEmpty()) {
            return ResponseEntity.ok(rooms);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/rooms/type")
    public ResponseEntity<List<Room>> findByRoomType(
            @RequestParam("roomType") String roomType
    ) {
        List<Room> rooms = roomService.findByRoomType(roomType);

        if (!rooms.isEmpty()) {
            return ResponseEntity.ok(rooms);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/rooms/numOfBed")
    public ResponseEntity<List<Room>> getRoomOfBed(@RequestParam("numOfBed") int numOfBed) {
        List<Room> rooms = roomService.numOfBed(numOfBed);

        if (!rooms.isEmpty()) {
            return ResponseEntity.ok(rooms);
        } else {
            return ResponseEntity.notFound().build();
        }

    }

    @GetMapping("/rooms/maxPeople")
    public ResponseEntity<List<Room>> getMaxPeople(@RequestParam("maxPeople") int maxPeople) {
        List<Room> rooms = roomService.numOfBed(maxPeople);

        if (!rooms.isEmpty()) {
            return ResponseEntity.ok(rooms);
        } else {
            return ResponseEntity.notFound().build();
        }

    }
}
