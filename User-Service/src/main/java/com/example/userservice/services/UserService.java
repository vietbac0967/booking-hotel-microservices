package com.example.userservice.services;

import com.example.userservice.exceptions.UserAlreadyExistsException;
import com.example.userservice.models.User;
import com.example.userservice.repositories.UserRepository;
import lombok.RequiredArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder encoder;


    public User createUser(User user) {
        final var userByPhone = userRepository.getUserByPhone(user.getPhone());
        final var userByEmail = userRepository.getUserByEmail(user.getEmail());
        final var userByCID = userRepository.getUserByCid(user.getCid());
        if (userByPhone.isPresent()) {
            throw new UserAlreadyExistsException("Phone number is exist!");
        }
        if (userByEmail.isPresent()) {
            throw new UserAlreadyExistsException("Email is exist");
        }
        if (userByCID.isPresent()) {
            throw new UserAlreadyExistsException("cid is exits");
        }
        user.setPassword(encoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    public boolean deleteUser(Long id) {
        if (userRepository.findById(id).isPresent()) {
            userRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public boolean updateUser(User user) {
        if (userRepository.findById(user.getId()).isPresent()) {
            userRepository.save(user);
            return true;

        }
        return false;
    }

    public User getUserById(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new UserAlreadyExistsException("Not found user"));
    }

    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final var userByEmail = userRepository.getUserByEmail(username);

        return userByEmail.map(UserInfoDetails::new)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }
}
