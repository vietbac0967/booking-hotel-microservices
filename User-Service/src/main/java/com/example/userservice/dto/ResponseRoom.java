package com.example.userservice.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResponseRoom {
    private String name;
    private String phone;
    private String address;
    private List<RoomDTO> rooms;
}
