package com.example.userservice.controllers;

import com.example.userservice.dto.*;
import com.example.userservice.exceptions.UserAlreadyExistsException;
import com.example.userservice.models.User;
import com.example.userservice.services.JWTService;
import com.example.userservice.services.UserService;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.RequiredArgsConstructor;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {
    private final JWTService jwtService;
    private final AuthenticationManager authenticationManager;
    private final UserService userService;
    private final RestTemplate restTemplate = new RestTemplate();
    private final String API_TRANSACTION = "http://localhost:8086/api/v1/transactions/user";
    private final String API_ROOM = "http://localhost:8080/api/v1/rooms/user";
//    private final String API_TRANSACTION = "http://transaction-service:8086/api/v1/transactions/user";
//    private final String API_ROOM = "http://room-service:8080/api/v1/rooms/user";


    @GetMapping
    public List<User> getUsers() {
        return userService.getUsers();
    }


    @PostMapping("/register")
    public User createUser(@RequestBody User user) {
        return userService.createUser(user);
    }

    @PostMapping("/login")
    public String authenticateAndGetToken(@RequestBody AuthRequest authRequest) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
        if (authentication.isAuthenticated()) {
            return jwtService.generateToken(authRequest.getUsername());
        } else {
            throw new UsernameNotFoundException("invalid user request !");
        }
    }


    @GetMapping("/rooms")
    public ResponseRoom getRoomsForUser(@RequestParam("userId") Long id){
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(API_ROOM)
                .queryParam("userId", id);
        User user = userService.getUserById(id);
        RoomDTO[] rooms = restTemplate.getForObject(builder.toUriString(), RoomDTO[].class);
        assert rooms != null;
        return ResponseRoom
                .builder()
                .name(user.getName())
                .address(user.getAddress())
                .phone(user.getPhone())
                .rooms(Arrays.asList(rooms)).build();
    }


    @GetMapping("/info")
    public User getUserById(@RequestParam("userId") Long id) {
        return userService.getUserById(id);
    }


    @PostMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") Long id) {
        if (userService.deleteUser(id)) {
            return "Delete User Success";
        }
        return "Delete User Fail!";
    }

    @PostMapping("/update")
    public String updateUser(@RequestBody User user) {
        if (userService.updateUser(user)) {
            return "Update User Success";
        }
        return "Update User Fail!";
    }

    @GetMapping("/transactions")
    @Retry(name = "transactionService")
    public ResponseUser getTransactionsForUser(@RequestParam("userId") Long id) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(API_TRANSACTION)
                .queryParam("userId", id);

        TransactionDTO[] transactionDTOS = restTemplate.getForObject(builder.toUriString(), TransactionDTO[].class);
        if (transactionDTOS == null) {
            throw new UserAlreadyExistsException("Not found transactions for user");
        }
        final var userById = userService.getUserById(id);
        return ResponseUser.builder()
                .phone(userById.getPhone())
                .name(userById.getName())
                .address(userById.getAddress())
                .cid(userById.getCid())
                .transactionDTOS(Arrays.asList(transactionDTOS))
                .build();


    }


}
