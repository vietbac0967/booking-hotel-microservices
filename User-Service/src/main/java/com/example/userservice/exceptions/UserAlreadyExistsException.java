package com.example.userservice.exceptions;

import lombok.NoArgsConstructor;

public class UserAlreadyExistsException extends RuntimeException{
    public UserAlreadyExistsException(String message) {
        super(message);
    }
}
