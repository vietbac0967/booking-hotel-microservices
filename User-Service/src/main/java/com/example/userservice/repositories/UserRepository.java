package com.example.userservice.repositories;

import com.example.userservice.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    Optional<User> getUserByPhone(String phone);
    Optional<User> getUserByEmail(String email);
    Optional<User> getUserByCid(String cid);
    Optional<User> getUserByName(String name);
}
