package com.example.transactionservice.dto;

import com.example.transactionservice.enums.TypeTransaction;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TransactionDTO {
    private TypeTransaction typeTransaction;
    private double price;
    private Long userId;
}
