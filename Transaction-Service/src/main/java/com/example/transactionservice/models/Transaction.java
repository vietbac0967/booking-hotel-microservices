package com.example.transactionservice.models;

import com.example.transactionservice.enums.TypeTransaction;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@RedisHash("Transactions")
public class Transaction implements Serializable {
    @Id
    private UUID id;
    @Indexed
    private double price;
    @Indexed
    private TypeTransaction typeTransaction;
    @Indexed
    private LocalDate createAt;
    @Indexed
    private Long userId;


}
