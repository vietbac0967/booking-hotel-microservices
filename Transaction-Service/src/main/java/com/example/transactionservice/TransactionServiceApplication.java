package com.example.transactionservice;

import com.example.transactionservice.models.Transaction;
import com.example.transactionservice.repositories.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication
public class TransactionServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransactionServiceApplication.class, args);
	}

//	@Autowired
//	TransactionRepository transactionRepository;
//	@Bean
//	CommandLineRunner commandLineRunner(){
//		return  args -> {
//			List<Transaction> transactions = transactionRepository.findByUserId(1L);
//			if(transactions.isEmpty()){
//				System.out.println("Not found transactions");
//			}else{
//				transactions.forEach(System.out::println);
//			}
//		};
//	}
}
