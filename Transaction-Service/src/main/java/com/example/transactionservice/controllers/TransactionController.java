package com.example.transactionservice.controllers;


import com.example.transactionservice.dto.TransactionDTO;
import com.example.transactionservice.models.Transaction;
import com.example.transactionservice.repositories.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/transactions")
public class TransactionController {
    private final TransactionRepository transactionRepository;
    @GetMapping
    public List<Transaction> getAll() {
        List<Transaction> transactions = new ArrayList<>();
        transactionRepository.findAll().forEach(transactions::add);
        return transactions;
    }

    @PostMapping
    public Transaction createTransaction(@RequestBody TransactionDTO request) {
        Transaction transaction = Transaction.builder()
                .price(request.getPrice())
                .typeTransaction(request.getTypeTransaction())
                .createAt(LocalDate.now())
                .userId(request.getUserId())
                .build();
        transactionRepository.save(transaction);
        return transaction;
    }

    @GetMapping("/user")
    public List<Transaction> getTransactionsByUserId(@RequestParam("userId") Long userId) {
        return transactionRepository.findByUserId(userId);
    }
    @DeleteMapping("/{id}")
    public String deleteTransaction(@PathVariable("id") UUID id){
        transactionRepository.deleteById(id);
        return "Success";
    }
    @PutMapping()
    public Transaction updateTransaction(@RequestBody Transaction transaction){
        return transactionRepository.save(transaction);
    }

}
