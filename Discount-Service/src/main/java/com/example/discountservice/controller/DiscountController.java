package com.example.discountservice.controller;


import com.example.discountservice.dto.DiscountDetail;
import com.example.discountservice.dto.UserDTO;
import com.example.discountservice.models.Discount;
import com.example.discountservice.repositories.DiscountRepository;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Optional;

import java.util.List;

@RestController
@RequestMapping("/api/v1/discounts")
@RequiredArgsConstructor
public class DiscountController {
    private final DiscountRepository discountRepository;
    private final String API_USER = "http://localhost:8085/api/v1/users/info";
//    private final String API_USER = "http://user-service:8085/api/v1/users/info";
    private final RestTemplate restTemplate = new RestTemplate();


    @GetMapping
    @RateLimiter(name = "discountService")
    public List<Discount> getDiscounts() {
        return discountRepository.findAll();
    }

    @GetMapping("/{id}")
    public Discount getDiscount(@PathVariable("id") Long id) {
        return discountRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Discount not found for id: " + id));
    }

    @GetMapping("/detail/{id}")
    @RateLimiter(name = "userService")
    public DiscountDetail getDiscountDetailForUser(@PathVariable("id") Long id) throws Exception {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(API_USER)
                .queryParam("userId", id);
        UserDTO userDTO = restTemplate.getForObject(builder.toUriString(), UserDTO.class);
        Discount discount = discountRepository.findTopByUserId(id).orElseThrow(() -> new RuntimeException("Not found discount"));
        if (userDTO == null) {
            throw new Exception("Not found user");
        }
        return DiscountDetail.builder()
                .username(userDTO.getName())
                .phone(userDTO.getPhone())
                .discountName(discount.getDiscountName())
                .description(discount.getDescription())
                .startDate(discount.getStartDate())
                .endDate(discount.getEndDate())
                .discount(discount.getDiscount())
                .build();
    }

    @PostMapping
    public Discount createDiscount(@RequestBody Discount discount) {
        return discountRepository.save(discount);
    }

    @DeleteMapping("/{id}")
    public String deleteDiscount(@PathVariable("id") Long id) {
        if (discountRepository.existsById(id)) {
            discountRepository.deleteById(id);
            return "Delete Success";
        }
        return "Delete Fail!";
    }

    @PutMapping()
    public Discount updateDiscount(@RequestBody Discount discount) {
        return discountRepository.save(discount);
    }




}
