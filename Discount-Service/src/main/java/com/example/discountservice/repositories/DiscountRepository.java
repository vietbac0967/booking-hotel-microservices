package com.example.discountservice.repositories;

import com.example.discountservice.models.Discount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DiscountRepository extends JpaRepository<Discount,Long> {
    Optional<Discount> findTopByUserId(Long id);
}
