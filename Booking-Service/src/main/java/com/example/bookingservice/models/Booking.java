package com.example.bookingservice.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date bookingDate;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date checkInDate;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date checkOutDate;
    private int numberOfPeople;
    private int numberOfRoom;
    private Long userId;
}
