package com.example.bookingservice.controllers;

import com.example.bookingservice.dto.BookingInfoResponse;
import com.example.bookingservice.dto.UserDTO;
import com.example.bookingservice.models.Booking;
import com.example.bookingservice.repositories.BookingRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.http.HttpHeaders;
import java.util.List;

@RestController
@RequestMapping("/api/v1/booking")
@RequiredArgsConstructor
public class BookingController {
    private final BookingRepository bookingRepository;
     private final String API_USER = "http://localhost:8085/api/v1/users/info";
//    private final String API_USER = "http://user-service:8085/api/v1/users/info";
    private final RestTemplate restTemplate = new RestTemplate();
    private Booking tempBooking;

    @GetMapping
    public List<Booking> getAll() {
        return bookingRepository.findAll();
    }

    @PostMapping
    public Booking createBooking(@RequestBody Booking booking) {
        return bookingRepository.save(booking);
    }

    @DeleteMapping
    public String deleteBooking(@RequestParam("id") Long id) {
        if (bookingRepository.findById(id).isPresent()) {
            bookingRepository.deleteById(id);
            return "Delete Success";
        }
        return "User not found";
    }

    public BookingInfoResponse getBooking(Exception e) {
        return BookingInfoResponse.builder()
                .bookingDate(tempBooking.getBookingDate())
                .checkOutDate(tempBooking.getCheckInDate())
                .checkInDate(tempBooking.getCheckInDate())
                .numberOfPeople(tempBooking.getNumberOfPeople())
                .numberOfRoom(tempBooking.getNumberOfRoom())
                .build();
    }

    @GetMapping("/detail")
    @CircuitBreaker(name = "userService", fallbackMethod = "getBooking")
    public BookingInfoResponse getBookingInfoDetail(@RequestParam("id") Long id) {
        try {
            var bookingInfo = bookingRepository.findById(id);
            if (bookingInfo.isPresent()) {
                tempBooking = bookingInfo.get();
                UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(API_USER)
                        .queryParam("userId", bookingInfo.get().getUserId());

                UserDTO dto = restTemplate.getForObject(builder.toUriString(), UserDTO.class);
                assert dto != null;
                return BookingInfoResponse.builder()
                        .bookingDate(bookingInfo.get().getBookingDate())
                        .checkInDate(bookingInfo.get().getCheckInDate())
                        .checkOutDate(bookingInfo.get().getCheckOutDate())
                        .numberOfPeople(bookingInfo.get().getNumberOfPeople())
                        .numberOfRoom(bookingInfo.get().getNumberOfRoom())
                        .address(dto.getAddress())
                        .phone(dto.getPhone())
                        .username(dto.getName())
                        .build();
            }
            return null;
        } catch (Exception e) {
            return getBooking(e);
        }
    }
    // fall backend when request err


}
