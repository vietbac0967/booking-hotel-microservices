package com.example.bookingservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BookingInfoResponse {
    private Date bookingDate;
    private Date checkInDate;
    private Date checkOutDate;
    private int numberOfPeople;
    private int numberOfRoom;
    private String username;
    private String phone;
    private String address;
}
